package cat.escolapia.damviod.pmdm.tetris;

import android.media.audiofx.BassBoost;
import android.provider.Settings;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

/**
 * Created by daniel.mezo on 14/12/2016.
 */
public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g=game.getGraphics();
        Assets.background=g.newPixmap("background.jpg", Graphics.PixmapFormat.RGB565);
        Assets.startButton=g.newPixmap("start.png", Graphics.PixmapFormat.RGB565);
        Assets.logoMain=g.newPixmap("logo.png", PixmapFormat.RGB565);
        Assets.ready=g.newPixmap("ready.png", PixmapFormat.RGB565);
        Assets.backgroundGame=g.newPixmap("backgroundGame.jpg", PixmapFormat.RGB565);
        Assets.redShape=g.newPixmap("square.png", PixmapFormat.RGB565);
        Assets.blueShape=g.newPixmap("blueSquare.png", PixmapFormat.RGB565);
        Assets.greenShape=g.newPixmap("greenSquare.png", PixmapFormat.RGB565);
        Assets.yellowShape=g.newPixmap("yellowSquare.png", PixmapFormat.RGB565);
        Assets.blue2Shape=g.newPixmap("blue2Square.png", PixmapFormat.RGB565);
        Assets.purpleShape=g.newPixmap("purpleSquare.png", PixmapFormat.RGB565);
        Assets.orangeShape=g.newPixmap("orangeSquare.png", PixmapFormat.RGB565);
        Assets.numbers=g.newPixmap("numbers.png", PixmapFormat.RGB565);
        Assets.buttons=g.newPixmap("buttons.png", PixmapFormat.RGB565);
        Assets.pause=g.newPixmap("pause.png", PixmapFormat.RGB565);
        Assets.end=g.newPixmap("end.png", PixmapFormat.RGB565);
        game.setScreen(new MainMenuScreen(game));
    }

    @Override
    public void render(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
