package cat.escolapia.damviod.pmdm.tetris;

import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.impl.AndroidGame;

/**
 * Created by daniel.mezo on 14/12/2016.
 */
public class Tetris extends AndroidGame {
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }
}
