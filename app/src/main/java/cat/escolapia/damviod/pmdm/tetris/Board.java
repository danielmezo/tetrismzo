package cat.escolapia.damviod.pmdm.tetris;

import java.util.Random;
/**
 * Created by daniel.mezo on 14/12/2016.
 */
public class Board {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 10;
    static final float TICK_DECREMENT = 0.5f;

    public Tetris tetris;
    Random random=new Random();
    public boolean gameOver=false;
    int map[][] = new int[WORLD_WIDTH][WORLD_HEIGHT];
    public Piece currentPiece;
    public int  nextPiece;
    public int score = 0;
    public float tick=TICK_DECREMENT;
    public float tickTime=0;

    public Board() {
        Map();
       newPiece();

    }
    public void newPiece()
    {
        currentPiece=new Piece(random.nextInt(Piece.numShapes)+1);
        nextPiece=random.nextInt(7)+1;
    }
    public void update(float deltaTime) {
        if(gameOver) return;
        tickTime+=deltaTime;
        while(tickTime>tick)
        {
            tickTime-=tick;
            if(checkCanMoveDown("Down"))
            {
                currentPiece.y++;
                score++;
            }
            else
            {
                putPieceInMap();
                checkLines();
                newPiece();
            }
        }
    }
    public void Map()
    {
        for(int x=0;x<WORLD_WIDTH;x++)
        {
            for(int y=0; y<WORLD_WIDTH;y++)
            {
                map[x][y]=-1;
            }
        }
    }
    public boolean checkCanMoveDown(String direction)
    {
        boolean move=true;
        for(int x=0;x<currentPiece.shape.length;++x)
        {
            for(int y=0;y<currentPiece.shape[0].length;++y)
            {
                int mapX=currentPiece.x + x;
                int mapY=currentPiece.y+y;
                if(direction=="Down")
                {
                    if (!canMoveDown(mapX, mapY))
                        move = false;
                }
                if(direction=="Left")
                {
                    if (!canMoveLeft(mapX, mapY))
                        move = false;
                }
                if(direction=="Right")
                {
                    if (!canMoveRight(mapX, mapY))
                        move = false;
                }
            }
        }
        return move;
    }
    private boolean canMoveDown(int mapX, int mapY) {
        if(mapY<-1)
        {
            return true;
        }
        if(mapY+1 == WORLD_HEIGHT)
        {
            return false;
        }
        if(map[mapX][mapY+1] != -1)
        {
            return false;
        }
        return true;
    }
    private boolean canMoveRight(int mapX, int mapY) {
        if(mapX+1 == WORLD_WIDTH)
        {
            return false;
        }
        if(mapY<0) {
            return true;
        }
        if(map[mapX+1][mapY] != -1)
        {
            return false;
        }

        return true;
    }
    private boolean canMoveLeft(int mapX, int mapY) {
        if(mapX-1==-1)
        {
            return false;
        }
        if(mapY<0)
        {
            return true;
        }
        if(map[mapX-1][mapY]!=-1)
        {
            return false;
        }

        return true;
    }
    public void putPieceInMap()
    {
        for(int x=0;x<currentPiece.shape.length;++x)
        {
            for(int y=0;y<currentPiece.shape[0].length;++y)
            {
                if(currentPiece.shape[x][y])
                {
                    if(currentPiece.y+y<0)
                    {
                        gameOver=true;
                        return;
                    }
                    map[currentPiece.x+x][currentPiece.y+y]=currentPiece.type;
                }
            }
        }
        score +=100;
    }
    public void checkLines()
    {
        for(int y=0; y<WORLD_HEIGHT;y++)
        {
            boolean line=true;
            for(int x=0; x<WORLD_WIDTH;x++)
            {
                if(map[x][y]==-1)
                {
                    line = false;
                }
            }
            if(line==true)
            {
                PutDown(y);
            }
        }
    }
    public void PutDown(int i)
    {
        for(int x=0; x<WORLD_WIDTH;x++)
        {
            map[x][i] = -1;
        }
        for(int y=i-1;y>=0;y--)
        {
            for(int x=0; x<WORLD_WIDTH;x++)
            {
                map[x][y+1] = map[x][y];
            }
        }
        score += 150;
    }
}
