package cat.escolapia.damviod.pmdm.tetris;

import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.impl.AndroidGame;

/**
 * Created by daniel.mezo on 14/12/2016.
 */
public class Piece {
    public int x;
    public int y;
    public int type;
    public int pintarPiece;
    static public int numShapes=7;
    public boolean[][] shape;
    protected Pixmap graphic;
    public Piece(int paramType)
    {
        y=0;
        x = (Board.WORLD_WIDTH/2);
        type = paramType;
        setShape();
    }
    public void setShape()
    {
        if (type == 1) { //E.g: Type 1 => I
            y -= 2;
            shape = new boolean[1][4];
            shape[0][0] = true;
            shape[0][1] = true;
            shape[0][2] = true;
            shape[0][3] = true;
            graphic = Assets.redShape;
            pintarPiece=0;
        } else if (type == 2)
        {
            //E.g: Type 1 => L
            x-=1;
            y-=2;
            shape = new boolean[3][2];
            shape[2][0] = true;
            shape[0][1] = true;
            shape[1][1] = true;
            shape[2][1] = true;
            graphic = Assets.blueShape;
            pintarPiece=1;
        }
        else if(type==3)
        {   //J
            y-=2;
            x-=1;
            shape = new boolean[3][2];
            shape[0][0] = true;
            shape[0][1] = true;
            shape[1][1] = true;
            shape[2][1] = true;
            graphic=Assets.greenShape;
            pintarPiece=2;
        }
        else if(type==4)
        {//cuadrado
            y-=2;
            x-=1;
            shape=new boolean[2][2];
            shape[0][0]=true;
            shape[0][1]=true;
            shape[1][0]=true;
            shape[1][1]=true;
            graphic=Assets.yellowShape;
            pintarPiece=3;
        }
        else if(type==5)
        {//S
            y-=2;
            x-=1;
            shape=new boolean[3][2];
            shape[1][0]=true;
            shape[2][0]=true;
            shape[0][1]=true;
            shape[1][1]=true;
            graphic=Assets.orangeShape;
            pintarPiece=4;
        }
        else if(type==6)
        {//_|_
            y-=2;
            x-=1;
            shape=new boolean[3][2];
            shape[1][0]=true;
            shape[0][1]=true;
            shape[1][1]=true;
            shape[2][1]=true;
            graphic=Assets.purpleShape;
            pintarPiece=5;
        }
        else if(type==7)
        {//s del reves
            y-=2;
            x-=1;
            shape=new boolean[3][2];
            shape[0][0]=true;
            shape[1][0]=true;
            shape[1][1]=true;
            shape[2][1]=true;
            graphic=Assets.blue2Shape;
            pintarPiece=6;
        }
    }
    public void Rotate()
    {
       final int arrayDimension1 = shape.length;
        final int arrayDimension2 = shape[0].length;
        boolean[][] newShape = new boolean[arrayDimension2][arrayDimension1];

        for (int i = 0; i < arrayDimension1; i++) {
            for (int j = 0; j < arrayDimension2; j++) {
                newShape[arrayDimension2-j-1][i] = shape[i][j];
            }
        }
        shape = newShape;
    }
    public void turnLeft() {x--;}
    public void turnRight() {
        x++;
    }
    public void turnDown() {
        y++;
    }


}
