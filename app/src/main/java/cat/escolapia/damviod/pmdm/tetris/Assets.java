package cat.escolapia.damviod.pmdm.tetris;

import cat.escolapia.damviod.pmdm.framework.Pixmap;

/**
 * Created by daniel.mezo on 14/12/2016.
 */
public class Assets {
    public static Pixmap background;
    public static Pixmap pieces[];
    public static Pixmap startButton;
    public static Pixmap logoMain;
    public static Pixmap ready;
    public static Pixmap backgroundGame;
    public static Pixmap redShape;
    public static Pixmap blueShape;
    public static Pixmap greenShape;
    public static Pixmap yellowShape;
    public static Pixmap orangeShape;
    public static Pixmap purpleShape;
    public static Pixmap blue2Shape;
    public static Pixmap numbers;
    public static Pixmap buttons;
    public static Pixmap pause;
    public static Pixmap end;
}
